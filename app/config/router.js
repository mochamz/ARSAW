import React from 'react';
import { StackNavigator, TabNavigator, NavigationAction } from 'react-navigation';
import Icon from 'react-native-vector-icons/FontAwesome';

import Home from '../components/Home';
import SearchResult from '../components/SearchResult';
import SearchResultMap from '../components/SearchResultMap';
import DetailResult from '../components/DetailResult';


export const DefaultStack = StackNavigator({
    Home: {
        screen: Home,
        navigationOptions: {
            title: 'Home',
        },
    },
    SearchResult: {
        screen: SearchResult,
        navigationOptions: {
            title: 'SearchResult',
        },
    },
    SearchResultMap: {
        screen: SearchResultMap,
        navigationOptions: {
            title: 'SearchResultMap',
        },
    },
    DetailResult: {
        screen: DetailResult,
        navigationOptions: {
            title: 'DetailResult',
        },
    },
}, {
    headerMode: 'none',
});

export const Root = StackNavigator({
    ARSAW: {
        screen: DefaultStack,
        navigationOptions: {
            title: 'ARSAW',
        },
    },
}, {
    headerMode: 'none',
});