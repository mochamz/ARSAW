import React, { Component } from 'react';
import {
    Platform,
    StyleSheet,
    Image,
    View,
} from 'react-native';
import {
    StyleProvider,
    Container,
    Text,
    Content,
    Header,
    Left,
    Body,
    Right,
    Button,
    Icon,
    Title,
    Grid,
    Row,
    Col,
    Card,
    CardItem,
    Item,
    Input,
    Label,
    Picker,
} from 'native-base';
import getTheme from '../theme/components';
import customColor from '../theme/variables/customColor';
import Dimensions from 'Dimensions';
import { NavigationActions } from 'react-navigation';
import Geocoder from 'react-native-geocoder';
import RNGooglePlaces from 'react-native-google-places';
import Default from '../stores/Default';
import Money from '../stores/Money';
import { observer } from 'mobx-react';
import MultiSlider from '@ptomasroos/react-native-multi-slider';

const platform = Platform.OS;
const { width, height } = Dimensions.get('window');

var wisata_image = Default.t_wisata === 'active' ? require('../img/tempat_active.png') : require('../img/tempat_inactive.png');
var hotel_image = Default.hotel === 'active' ? require('../img/hotel_active.png') : require('../img/hotel_inactive.png');;
var resto_image = Default.t_makan === 'active' ? require('../img/restaurant_active.png') : require('../img/restaurant_inactive.png');;

@observer
export default class Home extends Component {
    constructor(props) {
        super(props);
        this.state = {
            wisata_image: wisata_image,
            hotel_image: hotel_image,
            resto_image: resto_image,
            radius: Default.radius,
            minPrice: Default.startPrice,
            maxPrice: Default.endPrice,
            sliderStartPrice: Default.startPrice,
            sliderEndPrice: Default.endPrice,
            sliderOneChanging: false,
            curLat: Default.curLat,
            curLng: Default.curLng,
            keyword: Default.keyword,
        };
    }
    render() {
        return (
            <StyleProvider style={getTheme(customColor)}>
                <Container style={{ backgroundColor: '#fff' }}>
                    <Header>
                        {/* <Left>
                            <Button transparent>
                                <Icon name='menu' />
                            </Button>
                        </Left> */}
                        <Body>
                            <Title>Beranda</Title>
                        </Body>
                        {/* <Right /> */}
                    </Header>
                    <Content padder>
                        <Grid style={{ marginTop: 20 }}>
                            <Label style={{ marginBottom: 10 }}>Pilih Lokasi</Label>
                            <Row>
                                <Card>
                                    <CardItem
                                        style={{ marginVertical: 12 }}
                                        button
                                        onPress={() => {
                                            this.changeChoose('wisata');
                                        }}>
                                        <Col style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                                            <Image
                                                source={this.state.wisata_image}
                                                style={{ height: (width - 10) / 6, width: (width - 10) / 6, flex: 1 }}
                                            />
                                            <Text style={{ marginTop: 10, fontSize: 12 }}>T. Wisata</Text>
                                        </Col>
                                    </CardItem>
                                </Card>
                                <Card>
                                    <CardItem
                                        style={{ marginVertical: 12 }}
                                        button
                                        onPress={() => {
                                            this.changeChoose('hotel');
                                        }}>
                                        <Col style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                                            <Image
                                                source={this.state.hotel_image}
                                                style={{ height: (width - 10) / 6, width: (width - 10) / 6, flex: 1 }}
                                            />
                                            <Text style={{ marginTop: 10, fontSize: 12 }}>Penginapan</Text>
                                        </Col>
                                    </CardItem>
                                </Card>
                                <Card>
                                    <CardItem
                                        style={{ marginVertical: 12 }}
                                        button
                                        onPress={() => {
                                            this.changeChoose('resto');
                                        }}>
                                        <Col style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                                            <Image
                                                source={this.state.resto_image}
                                                style={{ height: (width - 10) / 6, width: (width - 10) / 6, flex: 1 }}
                                            />
                                            <Text style={{ marginTop: 10, fontSize: 12 }}>T. Makan</Text>
                                        </Col>
                                    </CardItem>
                                </Card>
                            </Row>
                            <Grid style={{ marginTop: 20 }}>
                                <Col>
                                    <Label>Tempat saat ini : </Label>
                                    <Text>{this.state.curPos}</Text>
                                </Col>
                                <Col style={{ width: 65 }}>
                                    <Button style={{ marginLeft: 0, backgroundColor: '#f68a1f' }}
                                        onPress={() => { this.openSearchModal() }}>
                                        <Icon name='ios-locate-outline' />
                                    </Button>
                                </Col>
                            </Grid>
                            <Grid style={{ marginTop: 20 }}>
                                <Col style={{ flex: 1 }}>
                                    <Label>Radius</Label>
                                    <Picker
                                        iosHeader="Select one"
                                        mode="dropdown"
                                        selectedValue={this.state.radius}
                                        onValueChange={this.onRadiusChange.bind(this)}
                                    >
                                        <Item label="1 km" value="1" />
                                        <Item label="2 km" value="2" />
                                        <Item label="3 km" value="3" />
                                        <Item label="4 km" value="4" />
                                        <Item label="5 km" value="5" />
                                    </Picker>
                                </Col>
                                <Col style={{ flex: 2 }}>
                                    <Label>Kata Kunci</Label>
                                    <Item stackedLabel>
                                        <Input
                                            onChangeText={(text) => Default.keyword = text}
                                            selectTextOnFocus={true}
                                        />
                                    </Item>
                                    {/* <Label>Harga</Label>
                                    <Grid>
                                        <Col style={{ flex: 1, alignItems: 'flex-start' }}>
                                            <Text style={{ marginTop: 13, marginBottom: 10 }}>Rp {Money.AddCommas(parseInt(this.state.sliderStartPrice))}</Text>
                                        </Col>
                                        <Col style={{ flex: 1, alignItems: 'flex-end' }}>
                                            <Text style={{ marginTop: 13, marginBottom: 10 }}>Rp {Money.AddCommas(parseInt(this.state.sliderEndPrice))}</Text>
                                        </Col>
                                    </Grid>
                                    <MultiSlider
                                        min={this.state.minPrice}
                                        max={this.state.maxPrice}
                                        values={[this.state.sliderStartPrice, this.state.sliderEndPrice]}
                                        sliderLength={(((width - 10) / 3) * 2) - 13}
                                        onValuesChange={this.sliderOneValuesChange}
                                        step={10000}
                                    /> */}
                                </Col>
                            </Grid>
                        </Grid>
                        <Button block
                            onPress={(event) => {
                                return this._navigateTo('SearchResult');
                            }}
                            style={{ backgroundColor: '#f68a1f', marginTop: 50 }}>
                            <Text>Cari</Text>
                        </Button>
                        <View style={{ height: 20 }} />
                    </Content>
                </Container >
            </StyleProvider >
        );
    }

    _navigateTo(index, params) {
        this.props.navigation.navigate(index, params);
    }

    onRadiusChange(value) {
        Default.radius = value
        this.setState({
            radius: value
        });
    }

    changeChoose(type) {
        if (type === 'wisata') {
            Default.t_wisata = Default.t_wisata === 'active' ? 'inactive' : 'active';
        } else if (type === 'hotel') {
            Default.hotel = Default.hotel === 'active' ? 'inactive' : 'active';
        } else if (type === 'resto') {
            Default.t_makan = Default.t_makan === 'active' ? 'inactive' : 'active';
        }
        new_wisata_image = Default.t_wisata === 'active' ? require('../img/tempat_active.png') : require('../img/tempat_inactive.png');
        new_hotel_image = Default.hotel === 'active' ? require('../img/hotel_active.png') : require('../img/hotel_inactive.png');
        new_resto_image = Default.t_makan === 'active' ? require('../img/restaurant_active.png') : require('../img/restaurant_inactive.png');
        this.setState({
            wisata_image: new_wisata_image,
            hotel_image: new_hotel_image,
            resto_image: new_resto_image,
        });
    }

    sliderOneValuesChangeStart = () => {
        this.setState({
            sliderOneChanging: true,
        });
    }

    sliderOneValuesChange = (values) => {
        let newValues = [0, 1];
        newValues[0] = values[0];
        newValues[1] = values[1];

        this.setState({
            sliderStartPrice: newValues[0],
            sliderEndPrice: newValues[1],
        });

        Default.startPrice = newValues[0];
        Default.endPrice = newValues[1];
    }

    sliderOneValuesChangeFinish = () => {
        this.setState({
            sliderOneChanging: false,
        });
    }

    openSearchModal() {
        RNGooglePlaces.openPlacePickerModal()
            .then((place) => {
                this.setState({
                    lat: place.latitude,
                    lng: place.longitude,
                    curPos: place.address ? place.address.split(',')[0] + ', ' + place.address.split(',')[1] : 'tempat tidak teridentifikasi',
                });
                Default.curLat = place.latitude;
                Default.curLng = place.longitude;
            })
            .catch(error => console.log(error.message));
    }

    placeDetail(lat, lng) {
        Geocoder.geocodePosition({ lat, lng }).then((res) => {
            myAddress = res["0"].formattedAddress ? res["0"].formattedAddress.split(',')[0] + ', ' + res["0"].formattedAddress.split(',')[1] : 'tempat tidak teridentifikasi'
            this.setState({
                curPos: myAddress,
                interactionsComplete: false
            });
        });
    }

    componentDidMount() {
        this.watchID = navigator.geolocation.watchPosition((position) => {
            var lastPosition = JSON.stringify(position);
            this.setState({
                lat: position.coords.latitude,
                lng: position.coords.longitude
            });
            Default.curLat = position.coords.latitude;
            Default.curLng = position.coords.longitude;
            if (!this.state.curPos) {
                this.placeDetail(this.state.lat, this.state.lng);
            }
        }, {
                //GeoLocation settings go here
                timeout: 20000,   // 20 seconds until it fires a GPS error
                maximumAge: 10000,   // 10 seconds
                enableHighAccuracy: true,   //check frequently and accurately
                distanceFilter: 10     // don't update if user stays still though
            });

    }

    componentWillUnmount() {
        //Stop ‘watching’ the GPS when the user finishes with the component
        navigator.geolocation.clearWatch(this.watchID);
    }
}

const styles = StyleSheet.create({
    backgroundImage: {
        flex: 1,
        resizeMode: 'stretch', // 'cover' or 'stretch'
        height: (platform === 'ios') ? height - 119 : height - 135,
        width: width,
    },
    viewContainer: {
        flex: 1,
        flexDirection: "column",
        justifyContent: "center",
        alignItems: "center",
        backgroundColor: 'rgba(0, 0, 0, 0.6)',
    },
    container: {
        flex: 1,
        backgroundColor: '#FFFFFF',
    },
});