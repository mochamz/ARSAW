import React, { Component } from 'react';
import {
    Platform,
    StyleSheet,
    Image,
    View,
    TouchableOpacity,
    FlatList,
} from 'react-native';
import {
    StyleProvider,
    Container,
    Text,
    Content,
    Header,
    Left,
    Body,
    Right,
    Button,
    Icon,
    Title,
    Grid,
    Row,
    Col,
    Card,
    CardItem,
    H1,
} from 'native-base';
import getTheme from '../theme/components';
import customColor from '../theme/variables/customColor';
import Dimensions from 'Dimensions';
import { NavigationActions } from 'react-navigation';
import RNGooglePlaces from 'react-native-google-places';
import Default from '../stores/Default';
import Detail from '../stores/Detail';
import MapView from 'react-native-maps';
import { observer } from 'mobx-react';
import Geocoder from 'react-native-geocoder';
// import ShareMenu from 'react-native-share-menu';

const platform = Platform.OS;
const { width, height } = Dimensions.get('window');

@observer
export default class DetailResult extends Component {
    constructor(props) {
        super(props);
        this.state = {
            curLat: Default.curLat,
            curLng: Default.curLng,
            predictions: null,
            place: [],
            review: [],
            photo: [],
            listReview: <Text style={{ alignSelf: 'center' }}>Memuat review</Text>,
            // sharedText: null,
        };
    }
    render() {
        var text = this.state.sharedText;
        return (
            <StyleProvider style={getTheme(customColor)}>
                <Container style={{ backgroundColor: '#fff' }}>
                    <Header>
                        <Left>
                            <Button transparent
                                onPress={() => {
                                    this._goBack();
                                }}>
                                <Icon name='md-arrow-back' />
                            </Button>
                        </Left>
                        <Body>
                            <Title>Detil</Title>
                        </Body>
                        <Right>
                            <Button transparent
                                onPress={() => {
                                    console.log(Default.place_id);
                                }}>
                                <Icon name='md-share' />
                            </Button>
                        </Right>
                    </Header>
                    <Content>
                        <View>
                            <Text>Shared text: {text}</Text>
                        </View>
                        <Image style={{ height: width / 3, width: width, resizeMode: 'cover' }} source={{ uri: 'https://maps.googleapis.com/maps/api/place/photo?maxwidth=400&photoreference=' + this.state.place.photo_ref + '&key=' + Default.googleAPIKey }} />
                        <View style={{ backgroundColor: '#ddd', height: width / 5, padding: 10 }}>
                            <Col>
                                <Text style={{ fontWeight: 'bold' }}>{this.state.place.name}</Text>
                                <Text>{this.state.place.address ? this.state.place.address.split(',')[0] + ', ' + this.state.place.address.split(',')[1] : null}</Text>
                            </Col>
                        </View>
                        <View style={{ padding: 10 }}>
                            <Col>
                                <Text style={{ fontWeight: 'bold' }}>Review : </Text>
                                <FlatList
                                    data={this.state.review}
                                    renderItem={
                                        ({ item }) =>
                                            <Card style={{ shadowColor: 'transparent', borderRadius: 5 }}>
                                                <CardItem style={{ borderRadius: 5 }}>
                                                    <Grid style={{ padding: 5 }}>
                                                        <Row>
                                                            <Col style={{ flex: 2, flexDirection: "column", justifyContent: "flex-start" }}>
                                                                <H1 style={{ fontSize: 16, marginTop: -10, fontWeight: "bold" }}>{item.name}</H1>
                                                            </Col>
                                                            <Col style={{ flex: 1, flexDirection: "column", justifyContent: "flex-end" }}>
                                                                <Text style={{ alignSelf: 'flex-end', fontSize: 14 }}>Rating : {item.rating}</Text>
                                                            </Col>
                                                        </Row>
                                                        <Row style={{ marginTop: 10 }}>
                                                            <Text style={{ fontSize: 14 }}>{item.text}</Text>
                                                        </Row>
                                                    </Grid>
                                                </CardItem>
                                            </Card>
                                    }
                                    ListEmptyComponent={this.state.listReview}
                                />
                            </Col>
                        </View>
                    </Content>
                </Container >
            </StyleProvider >
        );
    }

    _navigateTo(index, params) {
        this.props.navigation.navigate(index, params);
    }

    _goBack() {
        this.props.navigation.goBack(null);
    }

    // componentWillMount() {
    //     var that = this;
    //     ShareMenu.getSharedText((text: string) => {
    //         if (text && text.length) {
    //             that.setState({ sharedText: text });
    //         }
    //     })
    // }

    componentDidMount() {
        this.getPlace();
    }

    getPlace() {
        return fetch(Default.serverAPI + '/detail?place_id=' + Detail.place_id)
            .then((response) => response.json())
            .then((responseJson) => {
                this.setState({
                    place: responseJson.place,
                    review: responseJson.review,
                    photo: responseJson.photo,
                })
                if (responseJson.review.length < 1) {
                    this.setState({
                        listReview: <Text style={{ alignSelf: 'center' }}>Belum ada review yang diberikan.</Text>
                    })
                }
                console.log(responseJson.review);
            })
            .catch((error) => { console.error(error); });
    }
}

const styles = StyleSheet.create({
    backgroundImage: {
        flex: 1,
        resizeMode: 'stretch', // 'cover' or 'stretch'
        height: (platform === 'ios') ? height - 119 : height - 135,
        width: width,
    },
    viewContainer: {
        flex: 1,
        flexDirection: "column",
        justifyContent: "center",
        alignItems: "center",
        backgroundColor: 'rgba(0, 0, 0, 0.6)',
    },
    container: {
        flex: 1,
        backgroundColor: '#FFFFFF',
    },
});