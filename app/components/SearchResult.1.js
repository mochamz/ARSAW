import React, { Component } from 'react';
import {
    Platform,
    StyleSheet,
    Image,
    View,
    TouchableOpacity,
} from 'react-native';
import {
    StyleProvider,
    Container,
    Text,
    Content,
    Header,
    Left,
    Body,
    Right,
    Button,
    Icon,
    Title,
    Grid,
    Row,
    Col,
} from 'native-base';
import getTheme from '../theme/components';
import customColor from '../theme/variables/customColor';
import Dimensions from 'Dimensions';
import { NavigationActions } from 'react-navigation';
import RNGooglePlaces from 'react-native-google-places';
import Default from '../stores/Default';
import MapView from 'react-native-maps';
import { observer } from 'mobx-react';
import Geocoder from 'react-native-geocoder';

const platform = Platform.OS;
const { width, height } = Dimensions.get('window');

@observer
export default class SearchResult extends Component {
    constructor(props) {
        super(props);
        this.state = {
            curLat: Default.curLat,
            curLng: Default.curLng,
            predictions: null,
        };
    }
    render() {
        return (
            <StyleProvider style={getTheme(customColor)}>
                <Container style={{ backgroundColor: '#fff' }}>
                    <Header>
                        <Left>
                            <Button transparent
                                onPress={() => {
                                    this._goBack();
                                }}>
                                <Icon name='md-arrow-back' />
                            </Button>
                        </Left>
                        <Body>
                            <Title>Hasil Pencarian</Title>
                        </Body>
                        <Right />
                    </Header>
                    <Content padder>
                        <Button
                            onPress={() => {
                                console.log(Default.dataPlace)
                            }}>
                            <Text>Check</Text>
                        </Button>
                    </Content>
                </Container >
            </StyleProvider >
        );
    }

    _navigateTo(index, params) {
        this.props.navigation.navigate(index, params);
    }

    _goBack() {
        this.props.navigation.goBack(null);
    }

    componentDidMount() {
        this.getPlace();
    }

    getPlace() {
        var t_wisata = Default.t_wisata === 'active' ? 'zoo|art_gallery|park|museum|movie_theater' : '';
        var hotel = Default.hotel === 'active' ? '|lodging' : '';
        var t_makan = Default.t_makan === 'active' ? '|restaurant|food|cafe' : '';
        var t_ibadah = Default.t_makan === 'active' ? '|mosque|church' : '';
        var pagetoken = '';
        var results = [];
        return fetch('https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=' + Default.curLat + ',' + Default.curLng + '&radius=' + (parseInt(Default.radius) * 1000) + '&type=' + t_wisata + hotel + t_makan + t_ibadah + '&keyword=&hasNextPage=true&nextPage()=true&sensor=false&key=' + Default.googleAPIKey + '&pagetoken=' + pagetoken)
            .then((response) => response.json())
            .then((responseJson) => {
                results = responseJson.results;
                var str = { xA: [] };
                var tmp = {};
                for (var key in responseJson.results) {
                    tmp[key] = responseJson.results[key];
                    str.xA.push(tmp);
                }
                console.log(tmp);
                console.log(str);
                console.log(responseJson);
                console.log(results);
                Default.dataPlace = responseJson.results;
            })
            // .then((responseJson) => { return responseJson.movies; })
            .catch((error) => { console.error(error); });
    }

    componentWillUnmount() {
        //Stop ‘watching’ the GPS when the user finishes with the component
        navigator.geolocation.clearWatch(this.watchID);
    }
}

const styles = StyleSheet.create({
    backgroundImage: {
        flex: 1,
        resizeMode: 'stretch', // 'cover' or 'stretch'
        height: (platform === 'ios') ? height - 119 : height - 135,
        width: width,
    },
    viewContainer: {
        flex: 1,
        flexDirection: "column",
        justifyContent: "center",
        alignItems: "center",
        backgroundColor: 'rgba(0, 0, 0, 0.6)',
    },
    container: {
        flex: 1,
        backgroundColor: '#FFFFFF',
    },
});