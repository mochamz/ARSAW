import React, { Component } from 'react';
import {
    Platform,
    StyleSheet,
    Image,
    View,
    TouchableOpacity,
    FlatList,
} from 'react-native';
import {
    StyleProvider,
    Container,
    Text,
    Content,
    Header,
    Left,
    Body,
    Right,
    Button,
    Icon,
    Title,
    Grid,
    Row,
    Col,
    Card,
    CardItem,
    H1,
    Tab,
    Tabs,
} from 'native-base';
import getTheme from '../theme/components';
import customColor from '../theme/variables/customColor';
import Dimensions from 'Dimensions';
import { NavigationActions } from 'react-navigation';
import RNGooglePlaces from 'react-native-google-places';
import Default from '../stores/Default';
import Detail from '../stores/Detail';
import MapView, { Marker, Callout } from 'react-native-maps';
import { observer } from 'mobx-react';
import Geocoder from 'react-native-geocoder';

const platform = Platform.OS;
const { width, height } = Dimensions.get('window');

@observer
export default class SearchResult extends Component {
    constructor(props) {
        super(props);
        this.state = {
            curLat: Default.curLat,
            curLng: Default.curLng,
            predictions: null,
            checkSearch: [],
            dataSearch: [],
            emptyComponent: <Text style={{ alignSelf: 'center' }}>Mencari tempat tujuan...</Text>,
        };
    }
    render() {
        return (
            <StyleProvider style={getTheme(customColor)}>
                <Container style={{ backgroundColor: '#fff' }}>
                    <Header>
                        <Left>
                            <Button transparent
                                onPress={() => {
                                    this._goBack();
                                }}>
                                <Icon name='md-arrow-back' />
                            </Button>
                        </Left>
                        <Body>
                            <Title>Hasil Pencarian</Title>
                        </Body>
                        <Right>
                            <Button transparent
                                onPress={() => {
                                    this._navigateTo('SearchResultMap');
                                }}>
                                <Icon name='md-pin' />
                            </Button>
                        </Right>
                    </Header>
                    <Content>
                        <Content padder>
                            <FlatList
                                data={this.state.dataSearch}
                                renderItem={
                                    ({ item }) =>
                                        <Card style={{ shadowColor: 'transparent', borderRadius: 5 }}>
                                            <CardItem style={{ borderRadius: 5 }}
                                                button
                                                onPress={(event) => {
                                                    Detail.place_id = item.id;
                                                    return this._navigateTo('DetailResult');
                                                }}>
                                                <Grid>
                                                    <Col style={{ flex: 1, flexDirection: "column", justifyContent: "flex-start" }}>
                                                        {this.initImage(item.photo_ref)}
                                                    </Col>
                                                    <Col style={{ flex: 2, flexDirection: "column", justifyContent: "center", padding: 10 }}>
                                                        <Col>
                                                            <H1 style={{ fontSize: 16, marginTop: -10 }}>{item.name}</H1>
                                                            <Text style={{ fontSize: 14 }}>{item.address ? item.address.split(',')[0] : null}</Text>
                                                        </Col>
                                                        <Col style={{ flex: 1, flexDirection: "column", justifyContent: "flex-end" }}>
                                                            <Text style={{ alignSelf: 'flex-end', fontSize: 14 }}>jarak : {parseInt(item.distance)} m</Text>
                                                        </Col>
                                                    </Col>
                                                </Grid>
                                            </CardItem>
                                        </Card>
                                }
                                ListEmptyComponent={this.state.emptyComponent}
                            />
                            <View style={{ height: 20 }} />
                        </Content>
                    </Content>
                </Container >
            </StyleProvider >
        );
    }

    _navigateTo(index, params) {
        this.props.navigation.navigate(index, params);
    }

    _goBack() {
        this.props.navigation.goBack(null);
    }

    componentDidMount() {
        this.getPlace();
    }

    getPlace() {
        return fetch(Default.serverAPI + '/find?t_makan=' + Default.t_makan + '&hotel=' + Default.hotel + '&t_wisata=' + Default.t_wisata + '&geo_lat=' + Default.curLat + '&geo_lng=' + Default.curLng + '&price_start=' + Default.startPrice + '&price_end=' + Default.endPrice + '&radius=' + parseInt(Default.radius) * 1000 + '&keyword=' + Default.keyword)
            .then((response) => response.json())
            .then((responseJson) => {
                // for (var key in responseJson.results) {
                //     var tmp = {};
                //     tmp = responseJson.results[key];
                //     Default.dataPlace.push(tmp);
                // }
                if (responseJson.length < 1) {
                    this.setState({
                        emptyComponent: <Text style={{ alignSelf: 'center' }}>Tempat yang anda inginkan tidak ditemukan.</Text>
                    })
                }
                this.setState({
                    dataSearch: responseJson
                })
                Default.dataSearch = responseJson;
                // console.log(responseJson);
                // if(responseJson.next_page_token){
                //     this.getPlace(responseJson.next_page_token);
                // }
            })
            .catch((error) => { console.error(error); });
    }

    initImage(photo = '') {
        if (photo) {
            return (
                <Image style={{ borderTopLeftRadius: 5, borderBottomLeftRadius: 5, height: ((width - 10) / 3) - 10, width: ((width - 10) / 3) - 10, resizeMode: 'cover' }} source={{ uri: 'https://maps.googleapis.com/maps/api/place/photo?maxwidth=400&photoreference=' + photo + '&key=' + Default.googleAPIKey }} />
            );
        } else {
            return (
                <Image style={{ borderTopLeftRadius: 5, borderBottomLeftRadius: 5, height: ((width - 10) / 3) - 10, width: ((width - 10) / 3) - 10, resizeMode: 'cover' }} source={require('../img/noimage.jpg')} />
            );
        }
    }
}

const styles = StyleSheet.create({
    backgroundImage: {
        flex: 1,
        resizeMode: 'stretch', // 'cover' or 'stretch'
        height: (platform === 'ios') ? height - 119 : height - 135,
        width: width,
    },
    viewContainer: {
        flex: 1,
        flexDirection: "column",
        justifyContent: "center",
        alignItems: "center",
        backgroundColor: 'rgba(0, 0, 0, 0.6)',
    },
    container: {
        flex: 1,
        backgroundColor: '#FFFFFF',
    },
    map: {
        flex: 1,
        width: width,
        height: height,
    },
    radius: {
        height: 50,
        width: 50,
        borderRadius: 50 / 2,
        overflow: 'hidden',
        backgroundColor: 'rgba(0, 122, 255, 0.1)',
        borderWidth: 1,
        borderColor: 'rgba(0, 122, 255, 0.3)',
        alignItems: 'center',
        justifyContent: 'center'
    },
    marker: {
        height: 20,
        width: 20,
        borderRadius: 20 / 2,
        overflow: 'hidden',
        backgroundColor: '#145769',
        borderWidth: 3,
        borderColor: 'white'
    },
    newMarker: {
        backgroundColor: "#fff",
        borderWidth: 1,
        borderColor: "#145769",
        padding: 5,
        borderRadius: 5,
    },
});