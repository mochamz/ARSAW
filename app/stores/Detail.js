import { action, extendObservable } from 'mobx';

class Detail {
    constructor() {
        this.reset();
    }
    @action reset() {
        extendObservable(this, {
            place_id: null,
        });
    }
}
export default new Detail();