import { action, extendObservable } from 'mobx';

class Default {
    constructor() {
        this.reset();
    }
    @action reset() {
        extendObservable(this, {
            serverAPI: 'http://3380c773.ngrok.io',
            googleAPIKey: 'AIzaSyDKmcDYu7_upsW2SZrK9pioXQMX5aYAj64',
            t_wisata: 'active',
            hotel: 'active',
            t_makan: 'active',
            t_ibadah: 'inactive',
            radius: '1',
            startPrice: 0,
            endPrice: 10000000,
            curLat: '-8.73599363',
            curLng: '115.16763926',
            keyword: '',
            dataSearch: [],
            dataPlace: [],
        });
    }
}
export default new Default();